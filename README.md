# Data Science Projects

Step-by-step process of applying Data Science techniques such as acquiring a data set, developing a hypothesis, cleaning, parsing, and applying statistical modeling and data analysis principles in order to create a predictive model.  Overall, these projects display skills in research design, data mining, data cleaning, exploratory data analysis, and machine learning in order to discover patterns in data. 

